
$(call inherit-product, vendor/gms/common-blobs.mk)

#===========PixelPROPERTIES===========#

# PixelProps
PRODUCT_PRODUCT_PROPERTIES += \
    DEVICE_PROVISIONED=1 \
    dalvik.vm.debug.alloc=0 \
    ro.com.android.dataroaming=false \
    ro.support_one_handed_mode=true \
    ro.storage_manager.show_opt_in=false \
    ro.opa.eligible_device=true \
    ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
    ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html \
    ro.com.google.clientidbase=android-google \
    ro.error.receiver.system.apps=com.google.android.gms \
    ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent

# SetupWizard
PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.rotation_locked=true \
    ro.setupwizard.mode=DISABLED \
    setupwizard.enable_assist_gesture_training=true \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.feature.day_night_mode_enabled=true \
    setupwizard.feature.portal_notification=true \
    setupwizard.theme=glif_v3_light

# Gestures
PRODUCT_PROPERTY_OVERRIDES += \
    ro.boot.vendor.overlay.theme=com.android.internal.systemui.navbar.gestural
	
#Ringstone
PRODUCT_PRODUCT_PROPERTIES += \
    ro.config.alarm_alert=Fresh_start.ogg \
    ro.config.notification_sound=Eureka.ogg \
    ro.config.ringtone=Your_new_adventure.ogg

# IME
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.ime.theme_id=5 \
    ro.com.google.ime.system_lm_dir=/product/usr/share/ime/google/d3_lms

#===========PixelPROPERTIES-END===========#

#===========PixelOVERLAYS===========#
	
# MainOverlays
PRODUCT_PACKAGES += \
    PixelLauncherCustomOverlay \
    PixelDocumentsUIGoogleOverlay \
    BuiltInPrintServiceOverlay \
    GooglePermissionControllerOverlay \
    PixelConfigOverlayCommon \
    TeleServiceOverlay \
    CaptivePortalLoginOverlay \
    GoogleWebViewOverlay \
    TelecomOverlay \
    CellBroadcastServiceOverlay \
    SettingsGoogleOverlay \
    SettingsGoogleOverlayPixel2021 \
    TelephonyProviderOverlay \
    ContactsProviderOverlay \
    PixelConfigOverlay2018 \
    SettingsProviderOverlay \
    TraceurOverlay \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlay2021 \
    PixelSetupWizardOverlay \
    PixelSetupWizardOverlay2019 \
    PixelSetupWizardOverlayActiveEdge \
    PixelFwResOverlay \
    GoogleConfigOverlay \
    SystemUIGoogleOverlay

#===========PixelOVERLAYS-END===========#

#===========PixelAPP&LIBS&FILES===========#

# framework
PRODUCT_PACKAGES += \
    com.google.android.dialer.support \
    com.android.hotwordenrollment.common.util \
    com.google.android.hardwareinfo

# System app
PRODUCT_PACKAGES += \
    GoogleExtShared \
    GooglePrintRecommendationService
#Deleted Pkg:    CaptivePortalLoginGoogle

# System priv-app
PRODUCT_PACKAGES += \
    DocumentsUIGoogle \
    GooglePackageInstaller \
    TagGoogle
#Deleted Pkg:NetworkPermissionConfigGoogle NetworkStackGoogle

# Product app
PRODUCT_PACKAGES += \
    arcore \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Chrome-Stub \
    DevicePolicyPrebuilt \
    Drive \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    GoogleTTS \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    Maps \
    MarkupGoogle \
    MicropaperPrebuilt \
    ModuleMetadataGoogle \
    NgaResources \
    Photos \
    PixelThemesStub \
    PixelWallpapers2021 \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SoundAmplifierPrebuilt \
    SoundPickerPrebuilt \
    SSRestartDetector \
    talkback \
    TrichromeLibrary-Stub \
    Videos \
    VoiceAccessPrebuilt \
    WebViewGoogle-Stub \
    YouTube \
    YouTubeMusicPrebuilt

# Product priv-app
PRODUCT_PACKAGES += \
    AndroidAutoStubPrebuilt \
    ANGLE \
    AppDirectedSMSService \
    DeviceIntelligenceNetworkPrebuilt \
    DevicePersonalizationPrebuiltPixel2021 \
    FilesPrebuilt \
    GCS \
    GoogleDialer \
    GoogleOneTimeInitializer \
    GoogleRestorePrebuilt \
    HardwareInfo \
    HotwordEnrollmentOKGoogleHEXAGON \
    HotwordEnrollmentXGoogleHEXAGON\
    MaestroPrebuilt \
    OTAConfigNoZeroTouchPrebuilt \
    PartnerSetupPrebuilt \
    Phonesky \
    PixelLiveWallpaperPrebuilt \
    PrebuiltBugle \
    PrebuiltGmsCore \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    AndroidPlatformServices \
    RecorderPrebuilt \
    SafetyHubPrebuilt \
    ScribePrebuilt \
    SecurityHubPrebuilt \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizardPrebuilt \
    TipsPrebuilt \
    TurboPrebuilt \
    Velvet \
    WellbeingPrebuilt

# System_Ext app
PRODUCT_PACKAGES += \
    EmergencyInfoGoogleNoUi	
# Deleted apps:Flipendo

# System_Ext priv-app
PRODUCT_PACKAGES += \
    GoogleServicesFramework \
    NexusLauncherRelease \
    PixelSetupWizard \
    StorageManagerGoogle \
    TurboAdapter \
    WallpaperPickerGoogleRelease

# Google APEX
#$(call inherit-product, vendor/gapps/apex.mk)

#===========PixelAPP&LIBS&FILES-END===========#
