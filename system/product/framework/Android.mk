LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := com.google.android.dialer.support
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_MODULE_TAGS := optional
LOCAL_PRODUCT_MODULE := true
LOCAL_MODULE_STEM := com.google.android.dialer.support.jar
LOCAL_SRC_FILES := $(LOCAL_MODULE).jar
LOCAL_ENFORCE_USES_LIBRARIES := false
LOCAL_DEX_PREOPT := true
#LOCAL_REQUIRED_MODULES := com.google.android.dialer.support
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := com.google.android.hardwareinfo
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_MODULE_TAGS := optional
LOCAL_PRODUCT_MODULE := true
LOCAL_MODULE_STEM := libhwinfo.jar
LOCAL_SRC_FILES := libhwinfo.jar
LOCAL_ENFORCE_USES_LIBRARIES := false
LOCAL_DEX_PREOPT := true
#LOCAL_REQUIRED_MODULES := com.google.android.hardwareinfo
include $(BUILD_PREBUILT)
