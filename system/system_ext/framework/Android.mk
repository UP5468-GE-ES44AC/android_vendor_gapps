LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := com.android.hotwordenrollment.common.util
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_MODULE_TAGS := optional
LOCAL_SYSTEM_EXT_MODULE := true
LOCAL_MODULE_STEM := com.android.hotwordenrollment.common.util.jar
LOCAL_SRC_FILES := $(LOCAL_MODULE).jar
LOCAL_ENFORCE_USES_LIBRARIES := false
#LOCAL_REQUIRED_MODULES := com.android.hotwordenrollment.common.util.jar
include $(BUILD_PREBUILT)
